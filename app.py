from flask import Flask
from flask_session import Session
import os

from config import config
from controller import MusicManager, Searcher, Crawler, Indexer, Parser
from model import MusicLibrary

app = Flask(__name__)

app.config['SECRET_KEY'] = os.urandom(32)
app.config['SESSION_TYPE'] = "filesystem"

Session(app)

# Searcher routes
app.add_url_rule('/', defaults={'category': None}, view_func=Searcher.as_view('index'))
app.add_url_rule('/<category>', view_func=Searcher.as_view('search'))

# MusicManager routes
app.add_url_rule('/registrar', defaults={'loading_type': None}, view_func=MusicManager.as_view('register'))
app.add_url_rule('/registrar/<loading_type>', view_func=MusicManager.as_view('add'))

if __name__ == '__main__':
    library = MusicLibrary()
    MusicManager.set_library(library)
    Searcher.set_library(library)
    crawler = Crawler()
    MusicManager.set_crawler(crawler)
    crawler.set_validator(lambda url: not library.is_song_registered(url))
    parser = Parser()
    MusicManager.set_parser(parser)
    indexer = Indexer()
    config.init()
    app.run()
