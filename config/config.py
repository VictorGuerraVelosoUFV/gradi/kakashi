import toml

cfg = None


def init():
    global cfg
    with open("config.toml") as file:
        cfg = toml.load(file)


def categories():
    return cfg["Categories"]["list"]


def first_category():
    return cfg["Categories"]["first"]


def project_name():
    return cfg["Project"]["name"]


def loading_types():
    return cfg["Registrar"]["types"]


def loading_templates():
    return cfg["Registrar"]["templates"]


def first_loading_type():
    return cfg["Registrar"]["first"]
