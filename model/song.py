from peewee import *
from .schema import db


class Song(Model):
    url = CharField(unique=True)
    cover = CharField()
    title = CharField()
    artist = CharField()
    lyrics = TextField()

    class Meta:
        database = db
