from peewee import *
from .schema import db
from .song import Song
from .term import Term


class TermFrequency(Model):
    term = ForeignKeyField(Term)
    song = ForeignKeyField(Song)
    type_tag = IntegerField()
    frequency = FloatField()

    class Meta:
        primary_key = CompositeKey('term', 'song', 'type_tag')
        database = db
