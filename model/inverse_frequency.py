from peewee import *
from .schema import db
from .term import Term


class InverseFrequency(Model):
    term = ForeignKeyField(Term)
    type_tag = IntegerField()
    idf = FloatField()

    class Meta:
        primary_key = CompositeKey('term', 'type_tag')
        database = db
