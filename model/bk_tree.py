from collections import deque


class BKTree:
    @staticmethod
    def _distance_func(s, t):
        m, n = len(s), len(t)
        v0 = [i for i in range(n + 1)]
        v1 = [0 for _ in range(n + 1)]
        for i in range(m):
            v1[0] = i + 1
            for j in range(n):
                delcost = v0[j + 1] + 1
                inscost = v1[j] + 1
                subcost = v0[j] if s[i] == t[j] else v0[j] + 1
                v1[j + 1] = min(delcost, inscost, subcost)
            v0, v1 = v1, v0
        return v0[n]

    def __init__(self):
        self._tree = None

    def add(self, id, s):
        if self._tree is None:
            self._tree = (s, id, {})
            return

        current, c_id, children = self._tree
        while True:
            dist = self._distance_func(s, current)
            if dist == 0:
                break
            target = children.get(dist)
            if target is None:
                children[dist] = (s, id, {})
                break
            current, c_id, children = target

    def search(self, s, radius):
        if self._tree is None:
            return []

        candidates = deque([self._tree])
        result = []
        while candidates:
            candidate, c_id, children = candidates.popleft()
            dist = self._distance_func(s, candidate)
            if dist <= radius:
                result.append((dist, c_id, candidate))

            low, high = dist - radius, dist + radius
            candidates.extend(c for d, c in children.items()
                              if low <= d <= high)
        return result
