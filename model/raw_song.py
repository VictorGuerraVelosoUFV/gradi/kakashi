class RawSong:
    def __init__(self, url, cover, title, artist, lyrics, kana=None):
        self.url = url
        self.cover = cover
        self.title = title
        self.artist = artist
        self.lyrics = lyrics
        self.kana = kana
