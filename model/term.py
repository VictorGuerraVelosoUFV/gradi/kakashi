from peewee import *
from .schema import db


class Term(Model):
    token = CharField()
    type_tag = IntegerField()

    class Meta:
        database = db
        indexes = (
            (('token', 'type_tag'), True),
        )
