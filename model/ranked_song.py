

class RankedSong:
    def __init__(self, url, cover, title, artist, lyrics, score=0.0):
        self.url = url
        self.cover = cover
        self.title = title
        self.artist = artist
        self.lyrics = lyrics
        self.score = score

    def __lt__(self, other):
        return self.score > other.score

    def raise_score(self, score):
        self.score += score
