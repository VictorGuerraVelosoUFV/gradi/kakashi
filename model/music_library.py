from peewee import *
from .schema import db
from .song import Song
from .term import Term
from .term_frequency import TermFrequency
from .inverse_frequency import InverseFrequency
from .bk_tree import BKTree
from .ranked_song import RankedSong
from .term_type import KANA, ROMAJI
from .term_frequency_type import TITLE, ARTIST, LYRICS
from math import log


class MusicLibrary:
    DB_CHUNK = 100
    SCORE_MUL = {
        KANA: { TITLE: 13, ARTIST: 7, LYRICS: 1 },
        ROMAJI: { TITLE: 1.8, ARTIST: 1.5, LYRICS: 1 }
    }

    @staticmethod
    def __get_ed_radius(token, ttype):
        l = len(token)
        if l > 8:
            l = 8
        return [[0, 0, 0, 1, 1, 1, 1, 2, 2], [0, 0, 0, 1, 1, 2, 2, 2, 3]][ttype][l]

    def __init__(self):
        print("[MusicLibrary] Initializing...")
        db.connect()
        db.create_tables([Song, Term, TermFrequency, InverseFrequency])
        self.__load_term_tree()
        db.close()

    def is_song_registered(self, url):
        db.connect()
        check = len(Song.select().where(Song.url == url)) > 0
        db.close()
        return check

    def register_song(self, song, terms):
        print("[MusicLibrary] Registering song...")
        db.connect()
        song.save()
        termfreq_ins = []
        updated_terms = []
        for term, freq_d in terms:
            term, created = Term.get_or_create(token=term.token,
                                               type_tag=term.type_tag)
            updated_terms.append(term)
            for type_tag, frequency in freq_d.items():
                termfreq_ins.append({
                    TermFrequency.term: term,
                    TermFrequency.song: song,
                    TermFrequency.type_tag: type_tag,
                    TermFrequency.frequency: frequency
                })
        with db.atomic():
            for batch in chunked(termfreq_ins, MusicLibrary.DB_CHUNK):
                TermFrequency.replace_many(batch).execute()
        self.__update_tree(updated_terms)
        db.close()

    def update_idf(self):
        print("[MusicLibrary] Updating IDF's...")
        db.connect()
        ndocs = len(Song.select())

        def idf(ndocst):
            return log(ndocs / (1 + ndocst), 2)

        query = (Term.select(Term, TermFrequency, fn.Count(Term.id).alias('n'))
                 .join(TermFrequency)
                 .group_by(Term, TermFrequency.type_tag))
        data = []
        for term in query.iterator():
            id = term.id
            tag = term.termfrequency.type_tag
            n = term.n
            data.append({InverseFrequency.term: id, 'type_tag': tag, 'idf': idf(n)})
        with db.atomic():
            for batch in chunked(data, MusicLibrary.DB_CHUNK):
                InverseFrequency.replace_many(batch).execute()
        db.close()

    def __update_tree(self, terms):
        for term in terms:
            self.__term_tree[term.type_tag].add(term.id, term.token)

    def __load_term_tree(self):
        self.__term_tree = [BKTree(), BKTree()]
        for id, token, type_tag in Term.select(Term.id, Term.token, Term.type_tag).tuples().iterator():
            self.__term_tree[type_tag].add(id, token)

    def __search_terms(self, token, ttype):
        return self.__term_tree[ttype].search(token, MusicLibrary.__get_ed_radius(token, ttype))

    def get_matching_songs(self, tokens, ftypes, ttype):

        def tfidf_query(token, ftype, ed):
            return (Term.select(Term.id, Song, TermFrequency.type_tag,
                    (TermFrequency.frequency * InverseFrequency.idf) / ed)
                .join(TermFrequency)
                .join(Song, on=(TermFrequency.song == Song.id))
                .join(InverseFrequency, on=(Term.id == InverseFrequency.term))
                .where(Term.token == token,
                       TermFrequency.type_tag == ftype,
                       InverseFrequency.type_tag == ftype)
                .group_by(Term.id, Song, TermFrequency.type_tag))

        songs_dict = {}
        for token in tokens:
            for ftype in ftypes:
                terms = self.__search_terms(token, ttype)
                for ed, id, token in terms:
                    query = tfidf_query(token, ftype, 1 + ed)
                    for row in query.tuples().iterator():
                        id, url, cover, title, artist, lyrics = row[1:7]
                        tfidf = row[8] * MusicLibrary.SCORE_MUL[ttype][ftype]
                        if id in songs_dict:
                            songs_dict[id].raise_score(tfidf)
                        else:
                            songs_dict[id] = RankedSong(url, cover, title,
                                                        artist, lyrics, tfidf)
        return sorted([song for song in songs_dict.values()])
