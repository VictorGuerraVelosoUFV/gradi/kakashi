from os import path
from functools import reduce
import re
import nagisa
from .transdict import TransDict
from model.raw_song import RawSong
from .pos_tags import *
from model import KANA, ROMAJI, TITLE, ARTIST, LYRICS


class Parser:
    __TAGS = {ADJ_NOUN, ADJECTIVE, ADNOMINAL, ADVERB, ALPHANUM, ENGLISH, NOUN, VERB, EXACT}

    @staticmethod
    def __iskana(s):
        return re.match('^[\u3040-\u30ff]*$', s) is not None

    def __init__(self):
        print("[Parser] Initializing...")
        dir = path.join(path.dirname(__file__), "data")
        self.__load_hira_kata(dir)
        self.__load_kata_roma(dir)
        self.__load_furigana(dir)
        self.__load_full_half()

    def __load_hira_kata(self, dir):
        print("[Parser] Loading hiragana to katakana converter...")
        with open(path.join(dir, 'hira-kata.dat')) as f:
            hira = f.readline()[:-1]
            kata = f.readline()[:-1]
            self.__hira_to_kata = str.maketrans(hira, kata)

    def __load_kata_roma(self, dir):
        print("[Parser] Loading katakana to romaji converter...")
        self.__kata_to_roma = TransDict()
        with open(path.join(dir, 'kata-roma.dat')) as f:
            for entry in f.read().split(",")[0:-1]:
                furi, roma = entry.split(" ")
                self.__kata_to_roma.add(furi, roma)

    def __load_furigana(self, dir):
        print("[Parser] Loading furigana...")
        self.__furigana = TransDict(False)
        with open(path.join(dir, 'dict.dat')) as f:
            for line in f.readlines():
                m = re.search(r'^(.+) \[(.*?)\]', line)
                if m is None:
                    continue
                entry, furi = m.group(1), m.group(2)
                if furi != '':
                    self.__furigana.add(entry, furi)
        print("[Parser] Loading names dictionary...")
        with open(path.join(dir, 'names.dat')) as f:
            for line in f.readlines():
                m = re.search(r'^(.+) \[(.*?)\]', line)
                if m is None:
                    continue
                entry, name = m.group(1), m.group(2)
                if name != '':
                    self.__furigana.add(entry, name)

    def __load_full_half(self):
        print("[Parser] Loading full-width to half-width converter...")
        full = ("ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ"
                "ａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚ"
                "０１２３４５６７８９　")
        half = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 "
        self.__full_to_half = str.maketrans(full, half)

    def extract(self, raw_song):
        print("[Parser] Extracting from raw song...")
        title = self.__extract_title(raw_song.title)
        artist = self.__extract_artist(raw_song.artist)
        lyrics, kana = self.__extract_kashi(raw_song.lyrics)
        return RawSong(raw_song.url, raw_song.cover, title, artist, lyrics, kana)

    def parse(self, raw_song):
        print("[Parser] Parsing raw song...")
        title_t = self.clean(self.tokenize(raw_song.title))
        artist_t = self.clean(self.tokenize(raw_song.artist))
        lyrics_t = self.clean(self.tokenize(raw_song.lyrics))
        kana_t = self.clean(self.__tokenize_kana(raw_song.kana))
        title_r = self.__romanize_any(title_t)
        artist_r = self.__romanize_any(artist_t)
        lyrics_r = self.__romanize_all_kana(kana_t)
        return ([(t, KANA, TITLE) for t in title_t]
                + [(t, ROMAJI, TITLE) for t in title_r]
                + [(t, KANA, ARTIST) for t in artist_t]
                + [(t, ROMAJI, ARTIST) for t in artist_r]
                + [(t, KANA, LYRICS) for t in lyrics_t]
                + [(t, ROMAJI, LYRICS) for t in lyrics_r])

    def __extract_title(self, raw):
        raw = raw.translate(self.__full_to_half)
        m = re.match('「(.*?)」', raw)
        return m.group(1) if m is not None else raw

    def __extract_artist(self, raw):
        raw = raw.translate(self.__full_to_half)
        m = re.match(r'^\s*(.*?)\s*$', raw)
        return m.group(1) if m is not None else raw

    def __extract_kashi(self, raw):
        raw = raw.translate(self.__full_to_half)
        m = re.match('<div.*?>\\s*([\s\S]*?)\\s*</div>$', raw)
        if m is None:
            return (None, None)
        text = m.group(1)
        text = re.sub("<br>", "", text)
        lyrics, kana = "", []
        pattern = re.compile(('(<span class="ruby"><span class="rb">(.*?)</span>'
                              '<span class="rt">(.*?)</span></span>)'))
        p = 0
        for m in re.finditer(pattern, text):
            i, j = m.span(1)
            prev = text[p:i]
            lyrics += prev + m.group(2)
            kana.append((prev, False))
            kana.append((m.group(3), True))
            p = j
        last = text[p:]
        lyrics += last
        kana.append((last, False))
        return lyrics, kana

    @staticmethod
    def tokenize(s):
        s = re.sub('\n', ' \n ', s)
        tokens = nagisa.tagging(s.lower())
        return zip(tokens.words, tokens.postags)

    def __tokenize_kana(self, kanas):
        return reduce(lambda a, b: a + b,
                      [[(k, EXACT)] if exact else list(self.tokenize(k))
                       for k, exact in kanas])

    @classmethod
    def clean(cls, tokens):
        cleaned_tokens = []
        for word, tag in tokens:
            if tag in cls.__TAGS and re.match('^\s+$', word) is None:
                cleaned_tokens.append(word)
        return cleaned_tokens

    def __romanize_all_furigana(self, s):
        return self.__kata_to_roma.transcript(s.translate(self.__hira_to_kata)).lower()

    def __romanize_all_kana(self, tokens):
        return [self.__romanize_all_furigana(token) for token in tokens]

    def __romanize_any(self, tokens):
        ftr = self.__romanize_all_furigana
        r = []
        for token in tokens:
            if self.__iskana(token):
                r.append(ftr(token))
            else:
                for entry in self.__furigana.transcript(token):
                    for furi in entry:
                        r.append(ftr(furi))
        return r
