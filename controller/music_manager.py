import json

from flask import session, render_template, request
from flask.views import MethodView

from config import config
from model import Song
from model.raw_song import RawSong
from .crawler import Crawler
from .parser import Parser
from .indexer import Indexer


class MusicManager(MethodView):

    def get(self, loading_type):
        card = None
        self._link_session_loading(loading_type)
        return render_template("song-registrar-template.html", **self._common_render_params(card))

    def post(self, loading_type):
        self._link_session_loading(loading_type)
        self._treat_operation()
        return render_template("song-registrar-template.html", **self._common_render_params(session["card"]))

    @classmethod
    def set_crawler(cls, c):
        cls.crawler = c

    @classmethod
    def set_parser(cls, p):
        cls.parser = p

    @classmethod
    def set_library(cls, l):
        cls.library = l

    @classmethod
    def _treat_operation(cls):
        if session["current_loading"] == config.loading_types()[0]:
            if request.values["type"] == "Fetch":
                session["card"] = cls._fetch_card()
            elif request.values["type"] == "Confirm":
                cls._confirm_song_insertion()
        elif session["current_loading"] == config.loading_types()[1]:
            cls._fetch_batch()
        elif session["current_loading"] == config.loading_types()[2]:
            cls._fetch_auto()

    @classmethod
    def _confirm_song_insertion(cls):
        kana = json.loads(request.values["kana"])
        print(kana)
        song = RawSong(request.values["song-url"], request.values["image"], request.values["name"],
                       request.values["artist"],
                       request.values["lyrics"], kana)
        cls._fetch([song])
        session["card"] = None

    @classmethod
    def _fetch_card(cls):
        print("[Manager] Fetching in single mode...")
        if "url" in request.values and len(request.values["url"]) > 0:
            try:
                raw_song = cls.crawler.fetch([request.values["url"]])[0]
            except IndexError:
                return None
            clean_raw_song = cls.parser.extract(raw_song)
            print(clean_raw_song.kana)
            return {  # TODO: Transformar em RawSong
                "song-url": clean_raw_song.url,
                "image": clean_raw_song.cover,
                "name": clean_raw_song.title,
                "artist": clean_raw_song.artist,
                "lyrics": clean_raw_song.lyrics,
                "kana": json.dumps(clean_raw_song.kana)
            }
        return None

    @staticmethod
    def _link_session_loading(loading_type):
        if loading_type is not None:
            session["current_loading"] = loading_type
        else:
            session["current_loading"] = config.first_loading_type()

    @staticmethod
    def _common_render_params(card):
        return {
            "name": config.project_name(),
            "loading_types": config.loading_types(),
            "loading_templates": config.loading_templates(),
            "zip": zip,
            "chosen": session["current_loading"],
            "title": "Register",
            "card": card
        }

    @classmethod
    def _fetch(cls, raw_songs):
        for raw_song in raw_songs:
            tokens = cls.parser.parse(raw_song)
            terms = Indexer.index(tokens)
            song = Song(url=raw_song.url,
                        cover=raw_song.cover,
                        title=raw_song.title,
                        artist=raw_song.artist,
                        lyrics=raw_song.lyrics)
            cls.library.register_song(song, terms)
        if len(raw_songs) > 0:
            cls.library.update_idf()

    @classmethod
    def _fetch_batch(cls):
        urls = request.values["urls"].split()
        print("[Manager] Fetching in batch mode...")
        raw_songs = [cls.parser.extract(song) for song in cls.crawler.fetch(urls)]
        print(len(raw_songs))
        cls._fetch(raw_songs)

    @classmethod
    def _fetch_auto(cls):
        limit = request.values["limit"]
        print("[Manager] Fetching in auto mode...")
        raw_songs = [cls.parser.extract(song) for song in cls.crawler.discover(limit)]
        print(len(raw_songs))
        cls._fetch(raw_songs)
