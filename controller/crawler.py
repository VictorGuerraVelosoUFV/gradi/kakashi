from scrapy import Spider
from scrapy.crawler import CrawlerProcess
from multiprocessing import Process, Queue
import re
from model.raw_song import RawSong
import random


class KashiFetcher:
    def __init__(self, then, validate):
        self.then = then
        self.validate = validate

    def fetch(self, response):
        url = response.url
        if self.validate(url):
            cover = response.css("div.lyricData__sub img::attr(src)").get()
            title = response.css("span.movieTtl_mainTxt::text").get()
            artist = response.css("a.boxArea_artists_move_top::text").get()
            kashi = response.css("div.hiragana").get()
            if title is not None and artist is not None and kashi is not None:
                self.then(RawSong(url, cover, title, artist, kashi))


class UtatenKashiSpider(Spider, KashiFetcher):
    name = 'utaten-kashi'
    domain = re.compile("https://utaten.com/lyric/.+/.+/?")

    def __init__(self, then, validate, urls, *args, **kwargs):
        Spider.__init__(self, *args, **kwargs)
        KashiFetcher.__init__(self, then, validate)
        domain = UtatenKashiSpider.domain
        self.__validate = validate
        self.start_urls = list(filter(lambda url: re.match(domain, url), urls))

    def parse(self, response):
        self.fetch(response)


class UtatenSearchSpider(Spider, KashiFetcher):
    name = 'utaten-search'
    start_urls = [("https://utaten.com/search/=/title=/artist_name="
                   "/sub_title=/lyricist=/composer=/beginning=/body=/tag="
                   "/sort=popular_sort%3Aasc/page={}/".format(random.randint(5, 80)))]
    xpaths = {
        'link': "//table/tr/td/p[@class='searchResult__title']/a/@href",
        'next-bar': "//nav[@class='pager']/ul",
        'item-current': "li.pager__item--current",
        'item-link': "a/@href"
    }

    def __init__(self, then, validate, *args, **kwargs):
        Spider.__init__(self, *args, **kwargs)
        KashiFetcher.__init__(self, then, validate)

    def parse(self, response):
        select = UtatenSearchSpider.xpaths
        for kashi_page in response.xpath(select['link']).getall():
            yield response.follow(kashi_page, self.fetch)
        next_bar = response.xpath(select['next-bar'])
        if len(next_bar) > 0:
            next_i = 6
            next_links = next_bar[0].xpath("li")[1:6]
            for i, s in enumerate(next_links):
                if s.css(select['item-current']).get() is not None:
                    next_i = i + 1
            if next_i < 6:
                next = next_links[next_i].xpath(select['item-link']).get()
                yield response.follow(next, self.parse)


class Crawler:
    def __init__(self, validator=None):
        print("[Crawler] Initializing...")
        self.__validator = validator

    def set_validator(self, validator):
        self.__validator = validator

    def __crawl(self, spider, *args, settings={}):
        print("[Crawler] Crawling...")
        validate = self.__validator or (lambda _: True)
        q = Queue()

        def crawl(settings, *args):
            crawler = CrawlerProcess(settings=settings)
            crawler.crawl(*args)
            crawler.start()
            then(None)

        def then(song):
            q.put(song)

        p = Process(target=crawl,
                    args=tuple([settings, spider, then, validate, *args]))
        p.start()
        songs = []
        while True:
            song = q.get()
            if song is None:
                break
            songs.append(song)
        p.join()
        return songs

    def discover(self, page_limit):
        settings = {'CLOSESPIDER_PAGECOUNT': page_limit}
        return self.__crawl(UtatenSearchSpider, settings=settings)

    def fetch(self, urls):
        return self.__crawl(UtatenKashiSpider, urls if type(urls) is list else [urls])
