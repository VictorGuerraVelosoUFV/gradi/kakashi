from model import Term, TITLE, ARTIST, LYRICS
from functools import reduce
from math import sqrt
from collections import defaultdict


class Indexer:
    @staticmethod
    def index(tokens):
        print("[Indexer] Indexing tokens...")
        freq = {}
        zero = lambda: 0
        norm_count = {
            TITLE: defaultdict(zero),
            ARTIST: defaultdict(zero),
            LYRICS: defaultdict(zero)
        }
        for token in tokens:
            key, ftype = token[0:2], token[2]
            norm_count[ftype][key] += 1
            if key in freq:
                d = freq[key]
                d[ftype] = d[ftype] + 1 if ftype in d else 1
            else:
                freq[key] = {ftype: 1}
        norm = {}
        for ftype, count in norm_count.items():
            seq = [v*v for v in count.values()]
            norm[ftype] = sqrt(reduce(lambda a, b: a + b, seq, 0))
        terms = []
        for token, f in freq.items():
            for ftype, v in f.items():
                f[ftype] = v / norm[ftype]
            terms.append((Term(token=token[0], type_tag=token[1]), f))
        return terms
