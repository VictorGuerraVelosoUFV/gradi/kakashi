from flask import session, request, render_template, redirect
from flask.views import MethodView

from config import config
from model import KANA, ROMAJI
from model.ranked_song import RankedSong
from .parser import Parser
from model import term_frequency_type


class Searcher(MethodView):
    RANGES = [
        {"from": ord(u"\u3300"), "to": ord(u"\u33ff")},  # compatibility ideographs
        {"from": ord(u"\ufe30"), "to": ord(u"\ufe4f")},  # compatibility ideographs
        {"from": ord(u"\uf900"), "to": ord(u"\ufaff")},  # compatibility ideographs
        {"from": ord(u"\U0002F800"), "to": ord(u"\U0002fa1f")},  # compatibility ideographs
        {'from': ord(u'\u3040'), 'to': ord(u'\u309f')},  # Japanese Hiragana
        {"from": ord(u"\u30a0"), "to": ord(u"\u30ff")},  # Japanese Katakana
        {"from": ord(u"\u2e80"), "to": ord(u"\u2eff")},  # cjk radicals supplement
        {"from": ord(u"\u4e00"), "to": ord(u"\u9fff")},
        {"from": ord(u"\u3400"), "to": ord(u"\u4dbf")},
        {"from": ord(u"\U00020000"), "to": ord(u"\U0002a6df")},
        {"from": ord(u"\U0002a700"), "to": ord(u"\U0002b73f")},
        {"from": ord(u"\U0002b740"), "to": ord(u"\U0002b81f")},
        {"from": ord(u"\U0002b820"), "to": ord(u"\U0002ceaf")}  # included as of Unicode 8.0
    ]

    def get(self, category):
        self._link_session_category(category)

        if self._is_luck_choice_selected():
            results = self._search_query(request.values["search"])
            url = results[0].url
            return redirect(url, code=302)
        elif self._is_search_result():  # self._is_common_search_selected(): TODO: Treat both cases separately
            session["results"] = self._search_query(request.values["search"])
            return render_template("search-result-template.html", **self._common_render_params(),
                                   search=request.values["search"], title="Results", n_results=len(session["results"]),
                                   results=session["results"])
        return render_template("search-template.html", **self._common_render_params(), title="Homepage")

    @staticmethod
    def _category_to_ftype(category):
        if category == config.categories()[0]:
            return [term_frequency_type.TITLE, term_frequency_type.ARTIST, term_frequency_type.LYRICS]
        elif category == config.categories()[1]:
            return [term_frequency_type.LYRICS]
        elif category == config.categories()[2]:
            return [term_frequency_type.TITLE]
        elif category == config.categories()[3]:
            return [term_frequency_type.ARTIST]

    @classmethod
    def set_library(cls, l):
        cls.library = l

    @staticmethod
    def _common_render_params():
        return {
            "name": config.project_name(),
            "categories": config.categories(),
            "chosen": session["current_category"]
        }

    @classmethod
    def _is_common_search_selected(cls):
        return cls._is_search_result() and request.values["type"] == "{} Search".format(config.project_name())

    @classmethod
    def _is_luck_choice_selected(cls):
        return cls._is_search_result() and request.values["type"] == "I'm Feeling Lucky"

    @classmethod
    def _is_search_result(cls):
        return "search" in request.values and "type" in request.values

    @staticmethod
    def _link_session_category(category):
        if category is not None:
            session["current_category"] = category
        else:
            session["current_category"] = config.first_category()

    @classmethod
    def _search_query(cls, query):
        ftypes = cls._category_to_ftype(session["current_category"])

        def is_cjk(char):
            return any([range["from"] <= ord(char) <= range["to"] for range in Searcher.RANGES])

        if any([is_cjk(c) for c in query]):
            tokens = Parser.clean(Parser.tokenize(query))
            ttype = KANA
        else:
            tokens = query.split()
            ttype = ROMAJI
        return cls.library.get_matching_songs(tokens, ftypes, ttype)
