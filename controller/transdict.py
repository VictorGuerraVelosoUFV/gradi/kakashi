class TransDict:
    def __init__(self, deterministic=True, cat=None):
        self.__dict = {}
        self.__deterministic = deterministic
        self.__cat = cat
        if cat is None:
            def append(a, b):
                a.append(b)
                return a

            self.__cat = (lambda a, b: a + b) if self.__deterministic else append

    def __str__(self):
        return self.__dict.__str__()

    def add(self, key, val):
        d = self.__dict
        for k in key:
            if k not in d:
                d[k] = {}
            d = d[k]
        if self.__deterministic:
            d[None] = val
        elif None not in d:
            d[None] = [val]
        else:
            d[None].append(val)

    def transcript(self, s):
        l, d, i, buffer = len(s), self.__dict, 0, ""
        r = "" if self.__deterministic else []
        cat = self.__cat
        while i < l:
            k = s[i]
            buffer += k
            if k in d:
                i += 1
                d = d[k]
            elif None in d:
                r = cat(r, d[None])
                buffer = ""
                d = self.__dict
            else:
                i += 1
                r = cat(r, buffer)
                buffer = ""
                d = self.__dict
        return cat(r, d[None] if None in d else buffer)
