from controller import MusicManager, Indexer
from model import MusicLibrary, TITLE, ARTIST, LYRICS, KANA, ROMAJI
from sys import argv

library = MusicLibrary()
# manager = MusicManager()

print([(song.score, song.title, song.artist)
    for song in library.get_matching_songs(argv[1:], [TITLE, ARTIST, LYRICS], ROMAJI)])
